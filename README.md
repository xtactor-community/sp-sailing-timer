**Special use case**
# Sailing timer

Firmware mod to enable sailing timer feature.

![flow chart](/flow-chart.jpg)
**Flow chart info:**
You enter the program by a shortcut (f.i. letter 'T'). Once in the program, you are in the 'normal' timer mode. You can then start the timer, or go to the settings menu. Some features:
- SYNC: automatically jumps to the next full minute
- Preset 1 to 5: Those are set with the app, and give an amount of minutes (or seconds) the timer should countdown from. Could be 1 to 5 min f.i. ...
- LINEAR and REPEAT mode are to different settings: In LINEAR mode, your watch will count up after the start signal, while in REPEAT mode, it will automatically start counting down from the set amount of minutes ones the start was reached (useful during training sessions)


